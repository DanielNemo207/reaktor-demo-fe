import React, { Component } from 'react';

class PackageThumbnail extends Component {

    constructor(props) {
        super(props);
        this.state = {package: props.package, ind:props.ind};
        this.selectHandler = this.selectHandler.bind(this);
    }

    selectHandler () {
        if (typeof this.props.onSelect === 'function') {
            this.props.onSelect(this.state.ind);
        }
    }

    render() {
        return (
            <div className="Package_thumbnail">

                <div className="row h-100">
                    <div className="col-8 my-auto">
                        <div className="thumbnail">
                            <div className="caption">
                                <h5>
                                    { this.state && this.state.package && this.state.package.name }
                                </h5>
                                <p style={{marginBottom: 0}}>    
                                    Version: { this.state && this.state.package && this.state.package.version }
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-4 my-auto">
                        <p style={{justifyContent: 'center'}}>
                            <button className="btn btn-primary btn-sm" onClick={this.selectHandler}>Show Details</button>
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

export default PackageThumbnail