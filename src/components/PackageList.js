import React, { Component } from 'react';
import PackageThumbnail from './PackageThumbnail';

class PackageList extends Component {

    state = {
        packages: [],
        isLoading: true
    }

    constructor(props) {
        super(props);
        this.state = {
            packages: props.packages,
            isLoading: props.isLoading
        };
    }

    componentWillReceiveProps(props) {
        this.setState({packages: props.packages, isLoading: props.isLoading})
    }

    render() {
        return (
            <div className="Package_list">

                <div className="panel panel-primary">
                    <div className="panel-heading">
                        <h3 className="panel-title">List of Packages</h3>
                    </div>
                    <div className="panel-body border" style={{height: '550px'}}>

                        { this.state && this.state.isLoading && (
                            <div>
                                <div className="spinner-border text-primary m-5" role="status">
                                    <span className="sr-only">Loading...</span>
                                </div>
                                <h6>Loading...</h6>
                            </div>
                        )}

                        <ul className="list-group overflow-auto h-100">
                            
                            { this.state && this.state.packages && this.state.packages.map((pack, ind) => (
                                <li className="list-group-item">
                                    <PackageThumbnail package={pack} ind={ind} onSelect={this.props.onSelect}/>
                                </li>
                            ))}
                            
                        </ul>

                    </div>
                </div>

            </div>
        );
    }
}

export default PackageList