import React, { Component } from 'react';

class PackageInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {package: props.package};
        this.navigateHandler = this.navigateHandler.bind(this);
    }

    navigateHandler (pack_name) {
        if (typeof this.props.onNavigate === 'function') {
            this.props.onNavigate(pack_name);
        }
    }

    componentWillReceiveProps(props) {
        this.setState({package: props.package})
    }

    render() {
        return (
            <div className="Package_info" style={{height: '700px'}}>
                
                <div className="panel panel-primary">

                    <div className="panel-heading">
                        <h3 className="panel-title">Details of Package</h3>
                    </div>

                    <div className="panel-body container border overflow-auto" style={{height: '550px'}}>
                        <div className="row m-auto p-auto pt-2">
                            <h4>
                                Name: { this.state && this.state.package && this.state.package.name }
                            </h4>
                        </div>
                        <div className="row m-auto p-auto">
                            <p>
                                Version: { this.state && this.state.package && this.state.package.version }
                            </p> 
                        </div>
                        <div className="row m-auto p-auto">
                            <h6>
                                Description:
                            </h6> 
                        </div>
                        <div className="row m-auto p-auto">
                            <p className="float-left">
                                { this.state && this.state.package && this.state.package.description }
                            </p> 
                        </div>
                        <div className="row m-auto p-auto">
                            <h6>
                                Dependencies:
                            </h6> 
                        </div>
                        <div className="row m-auto p-auto">
                            <p>
                                { this.state && this.state.package && this.state.package.depends && this.state.package.depends.map((pack, ind) => (
                                    <button type="button" className="btn btn-outline-success float-left" onClick={() => this.navigateHandler(pack)}>{pack}</button>
                                ))}
                            </p> 
                        </div>
                        <div className="row m-auto p-auto">
                            <h6>
                                Reverse Dependencies:
                            </h6> 
                        </div>
                        <div className="row m-auto p-auto">
                            <p>
                                { this.state && this.state.package && this.state.package.rev_depends && this.state.package.rev_depends.map((pack, ind) => (
                                    <button type="button" className="btn btn-outline-info float-left" onClick={() => this.navigateHandler(pack)}>{pack}</button>
                                ))}
                            </p> 
                        </div>
                    </div>

                </div>
                
            </div>
        );
    }
}

export default PackageInfo