import React, { Component } from 'react';
import { Package } from '../libs/Package';
import PackageList from './PackageList';
import PackageInfo from './PackageInfo';
import get_packages_url from '../ApiServices';

class Body extends Component {

    constructor(props) {
        super(props);
        this.state = {
            packages: [],
            selectingPackage: new Package(),
            isLoading: true
        };
    }

    componentDidMount() {
        this.setState({ isLoading: true });
        let state_packages = []

        fetch(get_packages_url)
            .then((response) => response.json())
            .then(packages_rep => {

                let packages = packages_rep['packages']

                for (var i = 0; i < packages.length; i++) {
                    let parsed_package = JSON.parse(packages[i])

                    let name = parsed_package['Package'];
                    let version = parsed_package['Version'];
                    let description = parsed_package['Description'];
                    let depends = parsed_package['Dependencies'];
                    let rev_depends = parsed_package['Rev-dependencies'];

                    let state_package = new Package()
                    state_package.name = name
                    state_package.version = version
                    state_package.description = description
                    state_package.depends = depends
                    state_package.rev_depends = rev_depends

                    state_packages.push(state_package)
                }

                this.setState({ packages: state_packages, isLoading: false });
        });
    }

    onSelect = (selected_index) => {
        let selectedPackage = this.state.packages[selected_index]

        this.setState({
            selectingPackage: selectedPackage
        });
    }

    onNavigate = (pack_name) => {
        let navigatedPackage = this.state.packages.find(pack => pack.name === pack_name);

        this.setState({
            selectingPackage: navigatedPackage
        });
    }

    render() {
        let { packages, selectingPackage, isLoading } = this.state;

        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-sm h-100">
                            <PackageList packages={packages} onSelect={this.onSelect} isLoading={isLoading}/>
                        </div>
                        <div className="col-sm h-100">
                            <PackageInfo package={selectingPackage} onNavigate={this.onNavigate}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Body