export class Package {

    constructor(name, version, description, depends, rev_depends) {
        this.name = name;
        this.version =  version;
        this.description = description;
        this.depends = depends;
        this.rev_depends = rev_depends;
    }

  }