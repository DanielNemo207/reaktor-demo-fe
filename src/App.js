import React, { Component } from 'react';
import './App.css';

import Header from './components/Header'
import Body from './components/Body'


class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />

        <div className="row top-buffer" style={{ height: '20px' }} />

        <Body />

      </div>
    );
  }
}

export default App;