const url = "https://reaktor-demo-be.herokuapp.com";
const proxyurl = "https://cors-anywhere.herokuapp.com/";

const prefix_url = proxyurl + url

const get_packages_route = "/packages";
const get_packages_url = prefix_url + get_packages_route;


export default get_packages_url